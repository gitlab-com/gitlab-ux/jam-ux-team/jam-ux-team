## Intro

This issue is a place for `@YOU` plan action items and track progress as a result of the professional development opportunities they're seeking while on GitLab. Use this issue to talk to your manager about your IGP plan, accomplishments, and iterations of your goals.

### How this issue works

It's simple! We'll use this issue to reference, reflect, and iterate on your goals. 

- Look at the Section 3 from the previous session and work with your manager to get started on your SMART goals.
- You can open and link issues to track actions you'll take as a result from things learned and resources used.
- Set calendar reminders where appropriate so you don’t forget to revisit your plan. Check items off directly in this issue as you complete each piece.
- Once a month, open a thread in this issue to discuss and record a summary of the progress made for the period and work with your manager to address any challenges.

## SMART goals

Choose [SMART goals](https://www.betterup.com/blog/smart-goals-examples) for your growth: they should be Specific, Measurable, Achievable, Relevant, and Time-bound. Keep the number of goals for each quarter low and track opportunities that will help achieve the goals you set. 

Consider using the following format: `(I or accountable party) will (action word/s) (object of the goal) by (time), in order to
(relevance/results).`

Remember to connect your SMART goal to a broader career goal. For example, if your career goal is "I want to learn how to be a leader in an IC role", your SMART goal can concentrate around developing new skills, complete a course, or be a mentee.

<!-- Example: I will complete and earn a certification in Crucial Conversations by June 31, 2024, in order to apply my conversation skills when having difficult conversations with my team and counterparts. -->

### Q1

- SMART goal:
- Career goal:
- Quarterly manager check-in:

### Q2

- SMART goal:
- Career goal:
- Quarterly manager check-in:

### Q3

- SMART goal:
- Career goal:
- Quarterly manager check-in:

#### [Talent Assessment](https://about.gitlab.com/handbook/people-group/talent-assessment/)

- [ ] Complete self-assessment
- [ ] Review results with my manager

### Q4

- SMART goal:
- Career goal:
- Quarterly manager check-in:

