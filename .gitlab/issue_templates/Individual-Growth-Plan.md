## Intro

Reflection helps guide our career decisions. This issue is the first part the [Individual Growth Plan](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/#individual-growth-plan) (IGP) for `@YOU` and outlines professional aspirations and goals. We'll use this issue to help guide the conversation between you and your manager.

### How this issue works

It's simple! Sections 1 and 2 are focused on self reflection and understanding your growth direction, while Section 3 is designed to help you think of your big career goals. After reviewing this issue with your manager, you'll create a measurable action plan on Section 4. This plan will ensure you're actively working on and consistently cognizant of your career development throughout the entire year. Find more guidance in the [Individual Growth Plan Guide](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/igp-guide) page.

## Section 1: Discover your inspiration

* **What parts of your role are most engaging?**
* **What work brings the greatest sense of excitement and belonging?**
* **What parts of your work energize you?**
* **What are you really good at?**
* **Where could you improve?**
* **Which areas of work feel draining?**
* **How do you like to learn?**
* **In 2-3 years, what role would you like to be in and why?**

## Section 2: Clarify your role

Now it's time to understand your [career motivations](https://www.kellogg.northwestern.edu/executive-education/the-kellogg-experience/thought-leadership/career-motivation.aspx). Knowing your growth direction helps you set goals and find the right resources. Go big in this section! Answer the following questions:

* **Do you want to stay on your current team or try something new? Why?**
* **Would you like to manage a team or department? Why?**
* **Would you like to stay as an individual contributor and develop a specific area of expertise? Why?**

### Manager check-in

Make sure you check off the following items before moving to Section 3:

- [ ] Individually, take some hours to reflect on what inspires you and what you'd like your role to look like. 
- [ ] Answer the questions outlined on Section 1 and 2.
- [ ] Schedule a time to review and discuss with your manager.

## Section 3: Set specific goals

After dicussing sections 1 and 2 with your manager, individually, choose 1-3 skills/behaviours from your Job Family and/or Job Framework to develop over the next 6 months. Consider how you like to learn and determine measurable and achievable action steps. 

| Skill/Behaviour | What resources/ support do you need? | How will this support your career goals? | List 1-3 measurable & achievable action steps | How will you engage your network? | Anticipated Challenges |
| ------ | ------ | ------ | ------ | ------ | ------ |
|        |        |        |        |        |        |
|        |        |        |        |        |        |
|        |        |        |        |        |        |

Check the [handbook](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/igp-guide/#section-3-set-specific-goals) for guidance on how to populate Section 3.

### Manager check-in

- [ ] Individually, take some hours to reflect on your goals.
- [ ] Populate Section 3.
- [ ] Schedule a time to review and discuss with your manager.

## Section 4: Creating an action plan

- [ ] Create an issue using the [Career Development Plan template](https://gitlab.com/gitlab-org/ci-cd-ux/-/blob/master/.gitlab/issue_templates/Career-Development-Plan.md). The new issue will serve as a place to take notes and plan action items as a result of the professional development opportunities you're seeking.
- [ ] Schedule a time to work with your manager to identify opportunities to develop your skills and knowledge. These areas for development should be relevant to both GitLab and your career.
